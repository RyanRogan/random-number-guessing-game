# 6x4=24 possibilities

from random import randint

print("The object of this game is to guess the random 4 digit number and the correct order of the digits. If you have given up, type h and the answer will be displayed. This will generate a new number. Good Luck.")

gamestate = "Start" # The state that the game is in. E.g. Start, over/exit
answer = list(str(randint(1000,9999))) # This is an answer that is randomly generated
attempts = 0 # This variable contains the number of attempts the user has made at guessing the answer. The dafault value is 0
#The main game loop function.
def gameLoop():
    # Teh global keyword turns these variables into globals so that they can be accessed in the function.
    global answer
    global attempts
    # This is the amount of correct digits that the user has guessed, default is 0
    CorrectDigits = 0

    userGuess = input("Guess the 4 digit number: ") # This allows the user to type in their guess
    userGuess = list(str(userGuess))
    if userGuess[0] == "h":
        print(str(answer)+"\nNew answer will be generated")
        answer = list(str(randint(1000,9999)))
    elif len(userGuess) != 4:
        print("Answer must have 4 digits")
    elif userGuess == answer:
        gamestate = "Over"
        print("You have won. Attempts: " + str(attempts) + "\nNew Game")
        answer = list(str(randint(1000,9999)))
    else:
        attempts = attempts + 1 # Number of user attempts insreases by 1

        # The following if statements checks how many correct digits the user has hguessed
        if userGuess[0] == answer[0] or userGuess[0] == answer[1] or userGuess[0] == answer[2] or userGuess[0] == answer[3]:
            CorrectDigits = CorrectDigits + 1
        if userGuess[1] == answer[0] or userGuess[1] == answer[1] or userGuess[1] == answer[2] or userGuess[1] == answer[3]:
            CorrectDigits = CorrectDigits + 1
        if userGuess[2] == answer[0] or userGuess[2] == answer[1] or userGuess[2] == answer[2] or userGuess[2] == answer[3]:
            CorrectDigits = CorrectDigits + 1
        if userGuess[3] == answer[0] or userGuess[3] == answer[1] or userGuess[3] == answer[2] or userGuess[3] == answer[3]:
            CorrectDigits = CorrectDigits + 1
        #This if statement checks the number of correct digits that the user guesses and decides if the word digit should be used as a plural or singular.
        digits = "digits"
        if CorrectDigits > 1:
            digits = "digits"
        print("Incorrect. You have " + str(CorrectDigits) + " correct "+digits+". Keep trying.")
# This loops the game loop function unti the gamestated is no longer equal to start
while gamestate == "Start":
    gameLoop()
    
