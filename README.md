This is a fun game that I made using python. The object of the game is to guess the random number that has been generated. 
The game is a work in progress and there are lots of different features that I want to add to it including a GUI. But for now
the game is CLI.

--
How to run:
Once you download the code, run it with python 3.6 or later.